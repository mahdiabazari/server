const bcrypt=require('bcryptjs');

let hashPassword=(password)=>new Promise((resolve,reject)=>{
    bcrypt.genSalt(10,(err,salt)=>{
        if(err) reject(err)
        bcrypt.hash(password,salt,(err,hash)=>{
            if(err) reject(err)
            resolve(hash)
        })
    })
})
let comparePass=(password,hashedPassword)=>new Promise((resolve,reject)=>{
    bcrypt.compare(password,hashedPassword,(err,success)=>{
        if(err) reject(err)
        else resolve(success);
    })
})

let comparePassword=async(password,hashedPassword)=> await bcrypt.compare(password,hashedPassword)


module.exports={comparePass,hashPassword,comparePassword}