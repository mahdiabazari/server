let redirectIfNotTeacher=(req,res,next)=>{
    if(req.session.teacher)next();
    else res.redirect("/")
}
let redirectIfNotStudent=(req,res,next)=>{
    if(req.session.user)next();
    else res.redirect("/")
}

module.exports={redirectIfNotTeacher,redirectIfNotStudent}