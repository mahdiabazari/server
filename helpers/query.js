const mysql = require('mysql');
let connection = mysql.createConnection({
    host : 'localhost',
    user: 'root',
    password: '',
    database: 'server'
})

let query=(query,values=undefined)=>new Promise((resolve,reject)=>{
    connection.query(query,values,(err,result,fields)=>{
        if(err) reject(err)
        resolve(result)
    })
})
module.exports=query