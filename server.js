const express = require('express');
var app = express();
const upload=require('./helpers/upload');
app.use(express.static('public'))
const hbs = require('hbs');
app.set('view engine', 'hbs');
const session = require('express-session');
const bodyParser = require('body-parser')
const path=require('path')
//create a function for executing queries
const query=require('./helpers/query');
//hash and compare password
const hash=require('./helpers/passwordHashing');
const auth=require('./helpers/Auth');


app.use(bodyParser.urlencoded({
    extended: true })
    ) 
app.use(session({
   secret: 'secret',
   resave: true,
   saveUninitialized: true
}))


app.get('/',(req,res)=>{
    res.render('login',{
    title: 'ورود',
    des: 'my site',
    
})
})


app.post('/auth',async(req,res)=>{
    let username = req.body.username;
    let password = req.body.password;
    let role = req.body.role;

    try{
        let user=await query('select * from user where username = ?',[username]);
        if(user.length>0) {
           //************************ */
            if(await hash.comparePassword(password,user[0].password)){   
            if(user[0].role=='user'){
                req.session.user=user
                res.redirect('/user')
            }
            
            if(user[0].role=='teacher'){
                req.session.teacher=user
                res.redirect("/teacher")
           }
        }
            //redirect to / if password is wrong
            else res.redirect("/")
        
        }
        //redirect to / if username not exist
        else res.redirect("/")
        
    }catch(err){
        res.redirect("/")
        
    }

});


// query("sdsadfsdfdf")
// .then(res=>query("sdfs",res))
// .then(res=>query("dfs",res))
// .then(res=>sfdsd)
app.post('/reg',async (req,res)=>{
    
    try{
        let username = req.body.username;
        let role = req.body.role;    
        let hashedPassword=await hash.hashPassword(req.body.password);
        
    
        console.log([username,hashedPassword,role])
        let user=await query("INSERT INTO user (username,password,role) VALUES (?,?,?)",[username,hashedPassword,role]);
        if(user){
            if(role== 'user'){
                req.session.user=username
                res.redirect('/user')
            }
            
            if(role=='teacher'){
                req.session.teacher=username
                res.redirect("/teacher")
            }
        }else res.redirect("/")
       

    }catch(err){
        res.redirect("/");
        throw(err)
    }
  

})



app.get("/teacher",auth.redirectIfNotTeacher,async(req,res)=>{

        try{
            let posts=await query("select * from posts");
            let countofcomments = await query('select count(*) from comment')
            // **********************^^^^
            // فرستادن اسم طرف به صفحه
            console.log(countofcomments)
            res.render("teacher",{posts,user:req.session.teacher[0],countofcomments})
            
            
        }catch(err){
            res.redirect("/user")
        }
    })





//********************************* */
app.get("/user",auth.redirectIfNotStudent,async(req,res) =>{
    let countofpost=await query("select count(*) from posts");
    let countofstudent=await query("select count(*) from user where role='user'");
    let countofteachesrs=await query("select count(*) from user where role='teacher'");
    let countofcomments=await query("select count(*) from comment");
    
    console.log(countofpost[0]['count(*)'])
    res.render("user",{countofpost:countofpost[0]['count(*)'] ,countofstudent ,countofteachesrs,countofcomments})
})
//////////////



app.get("/game" , auth.redirectIfNotStudent ,(req , res) =>{
    res.render('game')
})
app.post("/game" ,auth.redirectIfNotStudent ,(req , res) =>{
    res.redirect("/game")
})
app.post("/homepage" ,auth.redirectIfNotStudent ,(req , res) =>{
    res.redirect('/user')
})
app.post("/logout" , auth.redirectIfNotTeacher , (req , res) =>{
    res.redirect('/')
})
app.post("/newpost",auth.redirectIfNotTeacher,upload.single('image'), async (req,res)=>{
    let {title,category,text}=req.body;
    ////ارسال محتوا به جدول پست ها ^^^^
    let filePath=req.file.path.substr(6)
    let post_date=new Date()
    try{
        let post=await query("insert into posts (title,category,text,filepath,post_date) values(?,?,?,?,?)",[title,category,text,filePath,post_date]);
        location.reload(true)
    }catch(err){
        res.redirect("/teacher")
    }
})

app.post("/allposts" , auth.redirectIfNotStudent ,(req , res) =>{
    res.redirect('allposts')
} )
app.get('/allposts',async(req,res)=>{

    try{
        let posts=await query("select * from posts");
        let post_username = await query("select username from user where role = 'teacher'");
        //************** */
        console.log(post_username)
        res.render("allposts",{posts,post_username})
       //////*******************// */
    }catch(err){
        res.redirect("/user")
    }
})
app.get('/list_of_student',async(req , res) =>{
    try{
        let list=await query("select * from user where role='user'");
        res.render("list_of_student",{list})
    }catch(err){
        res.redirect("/teacher")
    }
} )


// ************* حذف پست^^^^^
app.post('/delete-post' ,auth.redirectIfNotTeacher,async(req , res) =>{
    let {id}=req.body;//****************^^^^^^^ */
    console.log(req.body)
    try{
        let delete_post=await query('delete from posts where id= ?',[id]);
        
        res.redirect('/teacher')
    }
    catch{
        console.log(err)
    }
})





// *****************************
// گرفتن ایدی طرفی که ورود کرده به سایت برای ذخیره پیام به اسم خودش
app.post('/send_message',async(req , res) =>{
    console.log(req.session.user[0].username)
    let text = req.body.text;
    console.log(text)
    let user = req.session.user[0].username
    try{
        let message=await query("insert into message (text,m.username) values(?,?)",[text,user]);
        res.redirect("/user")
        
    }catch(err){
        res.send("err")
    }
})
// *****************************************


app.get('/message' ,auth.redirectIfNotTeacher,async(req , res) =>{
    try{
        let message=await query("select * from message ");
        res.render("message",{message})
    }catch(err){
        res.redirect("/teacher")
    }
})

app.post('/likes' , async(req,res) =>{
    try{
    let {id}= req.body;
    console.log(req.session.user) 
    let likes = await query("UPDATE posts SET likes=likes +1 WHERE id=? " , [id]);
    location.reload(true)
    // res.render('allposts' , {req.session.user})
    //گرفتن سشن کسی که لایک کرده
    }
    catch{
        res.redirect("/allposts")
    }
})







/************************ */
app.get('/comments/:id' , async(req , res) =>{
    try{
        // let comments=await query("select * from comment ");
        // res.render("comments",{text , username})
        res.render('comments',{postId:req.params.id})
        
    }catch(err){
        res.redirect("/")
    }
})
// app.post('/post/:id')
app.post('/sendcomment', async(req , res) =>{
    try{
        let {text,postId}= req.body;
        let userId=req.session.user.id;
        console.log({userId,text,postId})
        let create=new Date();
        var accept;
        if(req.session.teacher){
           accept = 1
        }else{
           accept = 0
        }

        let comment = await query("insert into comment(username,text,created_at,post_id) values (?,?,?,?)",[username,text,create,postId]);
        location.reload(true)
        }
    
        catch{
            res.redirect("/allposts")
        }
})

// ************دیدن کامنت ها توسط استاد 
app.post('/view_comments/:id' ,async(req , res) =>{
    // let {id}=req.body;//**************** */
    console.log(req.params.id)
    try{
        res.render('comments',{postId:req.params.id})
        // let viewcomments=await query('select from comment where id= ?',[id]);
        
        // res.redirect('/comments',{viewcomments})
    }
    catch{
        console.log(err)
    }
})

app.listen(8000 , ()=> console.log('app is runing'))






//  let list=await query("select count(*) from user where role='user'");
//  res.render("message",{message,lists:list})
// }catch(err){
//     res.redirect("/teacher")
// }



// location.reload(true)